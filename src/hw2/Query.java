package hw2;

import java.util.ArrayList;
import java.util.List;

import hw1.Catalog;
import hw1.Database;
import hw1.HeapFile;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.parser.*;
import net.sf.jsqlparser.statement.*;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;

public class Query {

	private String q;
	
	public Query(String q) {
		this.q = q;
	}
	
	public Relation execute()  {
		Statement statement = null;
		try {
			statement = CCJSqlParserUtil.parse(q);
		} catch (JSQLParserException e) {
			System.out.println("Unable to parse query");
			e.printStackTrace();
		}
		Select selectStatement = (Select) statement;
		PlainSelect sb = (PlainSelect)selectStatement.getSelectBody();
		
		
		//your code here
		Catalog catalog = Database.getCatalog();
		// get FROM table and convert them to Relation of itself
		HeapFile fileOfFROM = catalog.getDbFile(catalog.getTableId(sb.getFromItem().toString()));
		Relation mainRelation = new Relation(fileOfFROM.getAllTuples(), fileOfFROM.getTupleDesc());
		// get JOIN tables and convert them to Relation of themselves
		List<Join> joins = sb.getJoins();
		if (joins != null) {
			for(Join join: joins) {
				int field1 = 0;
				int field2 = 0;
				String tableName = join.getRightItem().toString();
				HeapFile file = catalog.getDbFile(catalog.getTableId(tableName));
				Relation relation = new Relation(file.getAllTuples(), file.getTupleDesc());
				String[] on = new String[]{((EqualsTo)join.getOnExpression()).getLeftExpression().toString().toLowerCase(), ((EqualsTo)join.getOnExpression()).getRightExpression().toString().toLowerCase()};
				//match the column with table
				for(int i = 0; i < mainRelation.getDesc().numFields(); i++) {
					for(String s: on) {
						String[] names = s.split("\\.");
						if(names[1].compareToIgnoreCase(mainRelation.getDesc().getFieldName(i)) == 0) {
							field1 = i;
						}
					}
				}
				for(int i = 0; i < relation.getDesc().numFields(); i++) {
					for(String s: on) {
						String[] names = s.split("\\.");
						if(names[0].compareToIgnoreCase(tableName) == 0 && names[1].compareToIgnoreCase(relation.getDesc().getFieldName(i)) == 0) {
							field2 = i;
						}
					}
				}
				//do join operaiton
				mainRelation = mainRelation.join(relation, field1, field2);			
			}
		}
		
		// get WHERE
		Expression wherExpression = sb.getWhere();
		if(wherExpression != null) {
			WhereExpressionVisitor whereExpressionVisitor = new WhereExpressionVisitor();
			wherExpression.accept(whereExpressionVisitor);
			mainRelation = mainRelation.select(mainRelation.getDesc().nameToId(whereExpressionVisitor.getLeft().toLowerCase()), whereExpressionVisitor.getOp(), whereExpressionVisitor.getRight());
		}
		
		// get SELECT fields and project or aggregate
		ArrayList<Integer> projectedFields = new ArrayList<>();
		List<SelectItem> selectedItems = sb.getSelectItems();
		boolean groupBy = sb.getGroupByColumnReferences() == null ? false : true;
		boolean isAgg = false;
		if(!selectedItems.get(0).toString().equals("*")) {
			for(SelectItem item: selectedItems) {
				ColumnVisitor columnVisitor = new ColumnVisitor();
				item.accept(columnVisitor);
				if(!columnVisitor.getColumn().toString().equalsIgnoreCase("*")) projectedFields.add(mainRelation.getDesc().nameToId(columnVisitor.getColumn()));
				if (columnVisitor.isAggregate()) {
					if(projectedFields.size() > 0)
						mainRelation = mainRelation.project(projectedFields);
					mainRelation = mainRelation.aggregate(columnVisitor.getOp(), groupBy);
					isAgg = true;
				}  	

			}
			if(!isAgg) mainRelation = mainRelation.project(projectedFields);
		}
		// get the renamed fields
		ArrayList<Integer> renamedFields = new ArrayList<>();
		ArrayList<String> names = new ArrayList<>();
		for(SelectItem item: selectedItems) {
			if(item instanceof SelectExpressionItem) {
				SelectExpressionItem seItem = (SelectExpressionItem)item;
				if(seItem.getAlias() != null)
					if(seItem.getAlias().isUseAs()) {
						names.add(seItem.getAlias().getName());
						ColumnVisitor columnVisitor = new ColumnVisitor();
						item.accept(columnVisitor);
						renamedFields.add(mainRelation.getDesc().nameToId(columnVisitor.getColumn().toString()));
					}
			}
	
		}
		//if there is fields needing renamed, rename them
		if(renamedFields.size() > 0) {
			Relation finalR = mainRelation.rename(renamedFields, names);
			return finalR;
		}
		return mainRelation;
	}
}
