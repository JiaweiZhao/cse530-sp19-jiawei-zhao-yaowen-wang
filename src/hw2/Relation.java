package hw2;

import java.util.ArrayList;

import hw1.Field;
import hw1.RelationalOperator;
import hw1.Tuple;
import hw1.TupleDesc;
import hw1.Type;

/**
 * This class provides methods to perform relational algebra operations. It will be used
 * to implement SQL queries.
 * @author Doug Shook
 *
 */
public class Relation {

	private ArrayList<Tuple> tuples;
	private TupleDesc td;
	
	public Relation(ArrayList<Tuple> l, TupleDesc td) {
		//your code here
		this.tuples = l;
		this.td = td;
	}
	
	/**
	 * This method performs a select operation on a relation
	 * @param field number (refer to TupleDesc) of the field to be compared, left side of comparison
	 * @param op the comparison operator
	 * @param operand a constant to be compared against the given column
	 * @return
	 */
	public Relation select(int field, RelationalOperator op, Field operand) {
		//your code here
		ArrayList<Tuple> selectedTuples = new ArrayList<>();
		for(Tuple tuple : this.tuples){
			//find the one with condition
			if(tuple.getField(field).compare(op, operand))
				selectedTuples.add(tuple);
		}
		return new Relation(selectedTuples, this.td);
	}
	
	/**
	 * This method performs a rename operation on a relation
	 * @param fields the field numbers (refer to TupleDesc) of the fields to be renamed
	 * @param names a list of new names. The order of these names is the same as the order of field numbers in the field list
	 * @return
	 */
	public Relation rename(ArrayList<Integer> fields, ArrayList<String> names) {
		// your code here
		//if fields size < 0, just return this. if not, build new relations with the revised name fields
		if(names.size()==0) return this;
		Type[] types = new Type[this.td.numFields()];
		String[] newFieldNames = new String[this.td.numFields()];
		for(int i = 0; i < types.length; i++){
			types[i] = this.td.getType(i);
			newFieldNames[i] = this.td.getFieldName(i);
		}

		for(int i = 0; i < names.size(); i++){
			newFieldNames[fields.get(i)] = names.get(i);
		}
		TupleDesc newTupleDesc = new TupleDesc(types, newFieldNames);
		ArrayList<Tuple> renamedTuples = new ArrayList<>();
		for(Tuple tuple: this.tuples){
			Tuple renamedTuple = new Tuple(newTupleDesc);
			for(int i = 0; i < tuple.getDesc().numFields(); i++){
				renamedTuple.setField(i, tuple.getField(i));
			}
			renamedTuples.add(renamedTuple);
		}
		return new Relation(renamedTuples, newTupleDesc);
	}
	
	/**
	 * This method performs a project operation on a relation
	 * @param fields a list of field numbers (refer to TupleDesc) that should be in the result
	 * @return
	 */
	public Relation project(ArrayList<Integer> fields) {
		//your code here
		//just keep the fields projected
		Type[] projectedTypes = new Type[fields.size()];
		String[] projectedFieldNames = new String[fields.size()];
		for(int i = 0; i < fields.size(); i++){
			projectedTypes[i] = td.getType(fields.get(i));
			projectedFieldNames[i] = td.getFieldName(fields.get(i));
		}
		TupleDesc projectedDesc = new TupleDesc(projectedTypes, projectedFieldNames);
		ArrayList<Tuple> projectedTuples = new ArrayList<>();
		for(Tuple tuple: this.tuples){
			Tuple projectedTuple = new Tuple(projectedDesc);
			for(int i = 0; i < fields.size();i++){
					projectedTuple.setField(i, tuple.getField(fields.get(i)));
			}
			projectedTuples.add(projectedTuple);
		}
		return new Relation(projectedTuples, projectedDesc);
	}
	
	/**
	 * This method performs a join between this relation and a second relation.
	 * The resulting relation will contain all of the columns from both of the given relations,
	 * joined using the equality operator (=)
	 * @param other the relation to be joined
	 * @param field1 the field number (refer to TupleDesc) from this relation to be used in the join condition
	 * @param field2 the field number (refer to TupleDesc) from other to be used in the join condition
	 * @return
	 */
	public Relation join(Relation other, int field1, int field2) {
		//your code here
		int thisLen = this.getDesc().numFields();
		int otherLen = other.getDesc().numFields();
		int len = thisLen + otherLen;
		String[] joinedFieldNames = new String[len];
		Type[] joinedTypes = new Type[len];
		// append from this relation
		for(int i = 0; i < thisLen; i++){
			joinedFieldNames[i] = this.td.getFieldName(i);
			joinedTypes[i] = this.td.getType(i);
		}
		// append from the other relation
		for(int i = 0; i < otherLen; i++){
			joinedFieldNames[i + thisLen] = other.td.getFieldName(i);
			joinedTypes[i + thisLen] = other.td.getType(i);
		}
		// get new tuple desc for creating new tuple
		TupleDesc joinedTupleDesc = new TupleDesc(joinedTypes, joinedFieldNames);
		ArrayList<Tuple> joinedTuples = new ArrayList<>();

		// combine to get the joined tuple
		for(Tuple thisTuple: this.tuples){
			for(Tuple otherTuple: other.tuples){
				if(thisTuple.getField(field1).compare(RelationalOperator.EQ, otherTuple.getField(field2))){
					Tuple tuple = new Tuple(joinedTupleDesc);
					for(int i = 0; i < thisLen; i++){
						tuple.setField(i, thisTuple.getField(i));
					}
					for(int i = 0; i < otherLen; i++){
						tuple.setField(i + thisLen, otherTuple.getField(i));
					}
					joinedTuples.add(tuple);
				}
			}
		}
		
		return new Relation(joinedTuples, joinedTupleDesc);
	}
	
	/**
	 * Performs an aggregation operation on a relation. See the lab write up for details.
	 * @param op the aggregation operation to be performed
	 * @param groupBy whether or not a grouping should be performed
	 * @return
	 */
	public Relation aggregate(AggregateOperator op, boolean groupBy) {
		//your code here
		Aggregator aggregator = new Aggregator(op, groupBy, this.td);
		for(Tuple tuple: this.tuples){
			aggregator.merge(tuple);
		}

		return new Relation(aggregator.getResults(), this.td);
	}
	
	public TupleDesc getDesc() {
		//your code here
		return this.td;
	}
	
	public ArrayList<Tuple> getTuples() {
		//your code here
		return this.tuples;
	}
	
	/**
	 * Returns a string representation of this relation. The string representation should
	 * first contain the TupleDesc, followed by each of the tuples in this relation
	 */
	public String toString() {
		//your code here
		StringBuilder sb = new StringBuilder("Tuple description: " + this.td.toString() + "\n");
		for(Tuple tuple: this.tuples){
			sb.append(tuple.toString());
		}
		return sb.toString();
	}
}
