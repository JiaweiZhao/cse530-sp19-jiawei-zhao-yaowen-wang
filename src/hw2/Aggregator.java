package hw2;

import java.util.ArrayList;
import java.util.HashMap;

import hw1.IntField;
import hw1.RelationalOperator;
import hw1.Tuple;
import hw1.TupleDesc;

/**
 * A class to perform various aggregations, by accepting one tuple at a time
 * @author Doug Shook
 *
 */

public class Aggregator {


	// for groupBy == false
	private int count;
	private int sum;
	private ArrayList<Tuple> res;
	// for groupBy == true
	private HashMap<String, Integer> sumMap;
	private HashMap<String, Integer> countMap;
	private HashMap<String, ArrayList<Tuple>> resMap;


	private boolean groupBy;
	private AggregateOperator o;

	public Aggregator(AggregateOperator o, boolean groupBy, TupleDesc td) {
		//your code here
		count = 0;
		sum = 0;
		res = new ArrayList<>();
		countMap = new HashMap<>();
		resMap = new HashMap<>();
		sumMap = new HashMap<>();

		this.groupBy = groupBy;
		this.o = o;
	}

	/**
	 * Merges the given tuple into the current aggregation
	 * @param t the tuple to be aggregated
	 */
	public void merge(Tuple t) {
		//your code here
		if(!groupBy)
			count++;
		else{
			String key = t.getField(0).toString();
			if(countMap.containsKey(key)) countMap.put(key, countMap.get(key) + 1);
			else countMap.put(key, 1);
		}
			
		switch(this.o){
			case MAX: getMax(t); break;
			case MIN: getMin(t); break;
			case AVG: getAvg(t); break;
			case COUNT: getCount(t); break;
			case SUM: getSum(t);break;
		}
	}

	void getMax(Tuple t){
		if(!groupBy){
			if(res.size() == 0) res.add(t);
			else{
				if(res.get(0).getField(0).compare(RelationalOperator.LT, t.getField(0))){
					res.remove(0);
					res.add(t);
				}
			}
		} else {
			String key = t.getField(0).toString();
			if(resMap.containsKey(key)){
				if(resMap.get(key).get(0).getField(1).compare(RelationalOperator.LT, t.getField(1))){
					resMap.get(key).get(0).setField(1, t.getField(1));
				}
			} else {
				ArrayList<Tuple> list = new ArrayList<>();
				list.add(t);
				resMap.put(key, list);
			}
		}

	}
	void getMin(Tuple t){
		if(!groupBy){
			if(res.size() == 0) res.add(t);
			else{
				if(t.getField(0).compare(RelationalOperator.LT, res.get(0).getField(0))){
					res.remove(0);
					res.add(t);
				}
			}
		} else {
			String key = t.getField(0).toString();
			if(resMap.containsKey(key)){
				if(resMap.get(key).get(0).getField(1).compare(RelationalOperator.GT, t.getField(1))){
					resMap.get(key).get(0).setField(1, t.getField(1));
				}
			} else {
				ArrayList<Tuple> list = new ArrayList<>();
				list.add(t);
				resMap.put(key, list);
			}
		}

	}
	void getAvg(Tuple t){
		if(!groupBy){
			int value = ((IntField)t.getField(0)).getValue();
			sum += value;
			if(res.size() == 0) res.add(t);
			else res.get(0).setField(0, new IntField(sum/count));

		} else { 
			int value = ((IntField)t.getField(1)).getValue();
			String key = t.getField(0).toString();
			if(!resMap.containsKey(key)){
				ArrayList<Tuple> list = new ArrayList<>();
				list.add(t);
				resMap.put(key, list);
				sumMap.put(key, value);
			} else {
				int sum = sumMap.get(key) + value;
				sumMap.put(key, sum);
				resMap.get(key).get(0).setField(1, new IntField(sum/countMap.get(key)));;
			}
		}

	}

	void getCount(Tuple t){
		if(!groupBy){
			if(res.size() == 0)
				res.add(t);
			res.get(0).setField(0, new IntField(count));
		} else {
			String key = t.getField(0).toString();
			if(!resMap.containsKey(key)){
				ArrayList<Tuple> list = new ArrayList<>();
				t.setField(1, new IntField(1));
				list.add(t);
				resMap.put(key, list);
			} else {
				resMap.get(key).get(0).setField(1, new IntField(countMap.get(key)));
			}
		}
	
		
	}
	void getSum(Tuple t){
		if(!groupBy){
			if(res.size() == 0) res.add(t);
			else {
				int value = ((IntField)res.get(0).getField(0)).getValue();
				value += ((IntField)t.getField(0)).getValue();
				res.get(0).setField(0, new IntField(value));
			}
		} else { 
			String key = t.getField(0).toString();
			if(!resMap.containsKey(key)){
				ArrayList<Tuple> list = new ArrayList<>();
				list.add(t);
				resMap.put(key, list);
			} else {
				int value = ((IntField)resMap.get(key).get(0).getField(1)).getValue();
				value += ((IntField)t.getField(1)).getValue();
				resMap.get(key).get(0).setField(1, new IntField(value));;
			}
		}

	}
	/**
	 * Returns the result of the aggregation
	 * @return a list containing the tuples after aggregation
	 */
	public ArrayList<Tuple> getResults() {
		//your code here
		if(groupBy){
			for(String key: resMap.keySet()){
				res.add(resMap.get(key).get(0));
			}
		}
		return res;
	}

}
