package hw4;
import hw1.HeapPage;
import java.util.*;
public class Transaction {
	int id;
	HashSet<HeapPage> pagesToRead;
	HashSet<HeapPage> pagesToWrite;
	Transaction(int id, int tableID, int pageID){
		pagesToRead = new HashSet<>();
		pagesToWrite = new HashSet<>();
	}
	public void setLocks(HeapPage page, Permissions permission) {
		if(permission == Permissions.READ_ONLY) {
			if(pagesToWrite.contains(page)) pagesToWrite.remove(page);
			pagesToRead.add(page);
		}
		if(permission == Permissions.READ_WRITE) {
			if(pagesToRead.contains(page)) pagesToRead.remove(page);
			pagesToWrite.add(page);
		}
	}
	
}
