package hw4;

import java.awt.List;
import java.io.*;
import java.util.HashMap;

import hw1.Database;
import hw1.HeapPage;
import hw1.Tuple;

/**
 * BufferPool manages the reading and writing of pages into memory from
 * disk. Access methods call into it to retrieve pages, and it fetches
 * pages from the appropriate location.
 * <p>
 * The BufferPool is also responsible for locking;  when a transaction fetches
 * a page, BufferPool which check that the transaction has the appropriate
 * locks to read/write the page.
 */
public class BufferPool {
    /** Bytes per page, including header. */
    public static final int PAGE_SIZE = 4096;

    /** Default number of pages passed to the constructor. This is used by
    other classes. BufferPool should use the numPages argument to the
    constructor instead. */
    public static final int DEFAULT_PAGES = 50;
    
    
    
    HashMap<Integer, HashMap<Integer, HeapPage>> pageCache;
    HashMap<Integer, Transaction> transactions;

    /**
     * Creates a BufferPool that caches up to numPages pages.
     *
     * @param numPages maximum number of pages in this buffer pool.
     */
    public BufferPool(int numPages) {
    	this.pageCache = new HashMap<>();
    	this.transactions = new HashMap<Integer, Transaction>();
        // your code here
    }

    /**
     * Retrieve the specified page with the associated permissions.
     * Will acquire a lock and may block if that lock is held by another
     * transaction.
     * <p>
     * The retrieved page should be looked up in the buffer pool.  If it
     * is present, it should be returned.  If it is not present, it should
     * be added to the buffer pool and returned.  If there is insufficient
     * space in the buffer pool, an page should be evicted and the new page
     * should be added in its place.
     *
     * @param tid the ID of the transaction requesting the page
     * @param tableId the ID of the table with the requested page
     * @param pid the ID of the requested page
     * @param perm the requested permissions on the page
     */
    
    public HeapPage getPage(int tid, int tableId, int pid, Permissions perm)
        throws Exception {
        // your code here
    	
    	// if pool is full, evict a page.
    	if(this.getSize() >= DEFAULT_PAGES) {
    		evictPage();
    	}
    	// initialize a new HashMap for the required table if it is null
    	if(!pageCache.containsKey(tableId)) pageCache. put(tableId, new HashMap<Integer, HeapPage>());
    	// if page has existed in the cache, just return it; if not, copy from original table;
    	HeapPage page = pageCache.get(tableId).getOrDefault(pid, new HeapPage(pid, Database.getCatalog().getDbFile(tableId).readPage(pid).getPageData(), tableId));
    	
    	
    	// if a read lock is required
    	if(perm == Permissions.READ_ONLY) {
    		// set up timer
    		// set up lock
    		for (int i = 0; i < 10; i++) {
    			// if write lock is held by another transaction 
        		if(!(page.getWriteLock() == -1 || page.getWriteLock() == tid)) {
        			// wait.
        			try {
						Thread.sleep(100);
		    			if(i == 9) this.releasePage(tid, tableId, pid);

					} catch (Exception e) {
						// TODO: handle exception
					}
        			if(i == 9) this.transactionComplete(tid, false);
        			continue;
        		}
        		setUpLock(tid, tableId, pid, page, perm);
        		break;
			}
    	} 
    	
    	// if a write lock is required
    	if(perm == Permissions.READ_WRITE) {
    		// the only case that a write lock can be obtained:
    		// (no readLock or you hold the only lock) and write lock it not occupied.
    		if(page.getWriteLock() == tid) return page;
    		for(int i = 0; i < 10; i++) {
    			if(page.getWriteLock() == -1 && (page.getAllReadLocks().size() == 0 || (page.getAllReadLocks().size() == 1 && page.getAllReadLocks().contains(tid)))) {
    				setUpLock(tid, tableId, pid, page, perm);
    				break;
    			} else {
    				try {
						Thread.sleep(100);
					} catch (Exception e) {
						// TODO: handle exception
					}
    			}
    			if(i == 9) this.transactionComplete(tid, false);
    		}
    	}
    	// update the cache
    	pageCache.get(tableId).put(pid, page);
        return page;
    }
    private void setUpLock(int tid, int tableId, int pid, HeapPage page, Permissions perm) {
    	this.transactions.putIfAbsent(tid, new Transaction(tid, tableId, pid));
    	if(perm == Permissions.READ_ONLY) {
    		page.addReadLocks(tid);
    		if(page.getWriteLock() == tid) page.addWriteLock(-1);
    	}
    	if(perm == Permissions.READ_WRITE) {
    		page.addWriteLock(tid);
    		if (page.getAllReadLocks().contains(tid)) {
    			page.getAllReadLocks().remove(tid);
    		}
    	}
		this.transactions.get(tid).setLocks(page, perm);
    }

    /**
     * Releases the lock on a page.
     * Calling this is very risky, and may result in wrong behavior. Think hard
     * about who needs to call this and why, and why they can run the risk of
     * calling it.
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param tableID the ID of the table containing the page to unlock
     * @param pid the ID of the page to unlock
     */
    public  void releasePage(int tid, int tableId, int pid) {
        // your code here
    	HeapPage page = pageCache.get(tableId).get(pid);
    	if(page.getWriteLock() == tid) page.addWriteLock(-1);
    	if(page.getAllReadLocks().contains(tid)) page.getAllReadLocks().remove(tid);
    }

    /** Return true if the specified transaction has a lock on the specified page */
    public boolean holdsLock(int tid, int tableId, int pid) {
        // your code here
    	if(!pageCache.containsKey(tableId)) return false;
    	if(pageCache.get(tableId).containsKey(pid)) {
    		HeapPage page = pageCache.get(tableId).get(pid);
    		if(page.getWriteLock() == tid || page.getAllReadLocks().contains(tid))
    			return true;
    	}
    	
        return false;
    }

    /**
     * Commit or abort a given transaction; release all locks associated to
     * the transaction. If the transaction wishes to commit, write
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param commit a flag indicating whether we should commit or abort
     */
    public   void transactionComplete(int tid, boolean commit)
        throws IOException {
        // your code here
    		if(!transactions.containsKey(tid)) return;
    		Transaction transaction = transactions.get(tid);
    		for(HeapPage page : transaction.pagesToRead) {
    			page.getAllReadLocks().remove(tid);
    		}
    		for(HeapPage page : transaction.pagesToWrite) {
    			System.out.println();
    			page.addWriteLock(-1);
    			if(!commit) {
        			HeapPage originalPage = Database.getCatalog().getDbFile(page.getTableId()).readPage(page.getId());
        			HeapPage newPage = new HeapPage(page.getId(), originalPage.getPageData(), page.getTableId());
    				pageCache.get(newPage.getTableId()).put(page.getId(), newPage);
    			}
//    			else Database.getCatalog().getDbFile(page.getTableId()).writePage(page);
    			else this.flushPage(page.getTableId(), page.getId());
    		}
    		transactions.remove(tid);
    	
    	
    }

    /**
     * Add a tuple to the specified table behalf of transaction tid.  Will
     * acquire a write lock on the page the tuple is added to. May block if the lock cannot 
     * be acquired.
     * 
     * Marks any pages that were dirtied by the operation as dirty
     *
     * @param tid the transaction adding the tuple
     * @param tableId the table to add the tuple to
     * @param t the tuple to add
     */
    public  void insertTuple(int tid, int tableId, Tuple t)
        throws Exception {
        // your code here
    	for(HeapPage page : transactions.get(tid).pagesToWrite) {
    		if(page.hasOpenSlots()) {
    			page.addTuple(t);
    			page.isDirty = true;
    			return;
    		}
    	}
    	if(Database.getCatalog().getDbFile(tableId).isFull()) {
    		HeapPage newPage = new HeapPage(Database.getCatalog().getDbFile(tableId).getNumPages(), new byte[PAGE_SIZE], tableId);
    		HeapPage page = this.getPage(tid, tableId, newPage.getId(), Permissions.READ_WRITE);
    		page.addTuple(t);
    		page.isDirty = true;
    		return;
    		
    	}
    	for(int i = 0; i < Database.getCatalog().getDbFile(tableId).getNumPages(); i++) {
    		HeapPage page = Database.getCatalog().getDbFile(tableId).readPage(i);
    		if(page.hasOpenSlots()) {
    			HeapPage tmpHeapPage = this.getPage(tid, tableId, i, Permissions.READ_WRITE);
    			if(tmpHeapPage.getWriteLock() == tid) {
    				tmpHeapPage.addTuple(t);
    				tmpHeapPage.isDirty = true;
    				return;
    			}
    		}
    	}
    	
    }

    /**
     * Remove the specified tuple from the buffer pool.
     * Will acquire a write lock on the page the tuple is removed from. May block if
     * the lock cannot be acquired.
     *
     * Marks any pages that were dirtied by the operation as dirty.
     *
     * @param tid the transaction adding the tuple.
     * @param tableId the ID of the table that contains the tuple to be deleted
     * @param t the tuple to add
     */
    public  void deleteTuple(int tid, int tableId, Tuple t)
        throws Exception {
    	HeapPage page = getPage(tid, tableId, t.getPid(), Permissions.READ_WRITE);
    	if(page.getWriteLock() == tid) {
    		page.isDirty = true;
    		page.deleteTuple(t);
    	}
        // your code here
    }
    /**
     * Flushes the page to disk to ensure dirty pages are updated on disk.
     */
    private synchronized  void flushPage(int tableId, int pid) throws IOException {
        // your code here
    	HeapPage page = pageCache.get(tableId).get(pid);
    	page.isDirty = false;
    	Database.getCatalog().getDbFile(tableId).writePage(pageCache.get(tableId).get(pid));
    	
    }

    /**
     * Discards a page from the buffer pool.
     */
    private synchronized  void evictPage() throws Exception {
    	for(int tableId : pageCache.keySet()) {
    		for(int pageId : pageCache.get(tableId).keySet()) {
    			if(pageCache.get(tableId).get(pageId).isDirty == false) {
    				pageCache.get(tableId).remove(pageId);
    				return;
    			}
    		}
    	}
    	throw new Exception();
        // your code here
    }

    private int getSize() {
    	int count = 0;
    	for(int i : pageCache.keySet())
    		count += pageCache.get(i).size();
    	return count;
    }
}
