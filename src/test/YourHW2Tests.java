package test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import hw1.Catalog;
import hw1.Database;
import hw1.HeapFile;
import hw1.TupleDesc;
import hw2.Relation;
import hw2.Query;
import hw1.IntField;
import hw1.Tuple;

public class YourHW2Tests {

	private HeapFile testhf;
	private TupleDesc testtd;
	private HeapFile ahf;
	private TupleDesc atd;
	private HeapFile bhf;
	private TupleDesc btd;

	private Catalog c;

	@Before
	public void setup() {
		
		try {
			Files.copy(new File("testfiles/test.dat.bak").toPath(), new File("testfiles/test.dat").toPath(), StandardCopyOption.REPLACE_EXISTING);
			Files.copy(new File("testfiles/A.dat.bak").toPath(), new File("testfiles/A.dat").toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.out.println("unable to copy files");
			e.printStackTrace();
		}
		
		c = Database.getCatalog();
		c.loadSchema("testfiles/test.txt");
		
		int tableId = c.getTableId("test");
		testtd = c.getTupleDesc(tableId);
		testhf = c.getDbFile(tableId);
		
		c = Database.getCatalog();
		c.loadSchema("testfiles/A.txt");
		
		tableId = c.getTableId("A");
		atd = c.getTupleDesc(tableId);
		ahf = c.getDbFile(tableId);
		
		c = Database.getCatalog();
		c.loadSchema("testfiles/B.txt");
		
		tableId = c.getTableId("B");
		btd = c.getTupleDesc(tableId);
		bhf = c.getDbFile(tableId);
	}
	
	/*
	 * Test if Relation class can get the correct Tuple Description
	 */
	@Test
	public void testAggregateQ() {
		Query q = new Query("SELECT SUM(a2) FROM A");
		Relation r = q.execute();
		
		assertTrue("Aggregations should result in one tuple",r.getTuples().size() == 1);
		IntField agg = (IntField) r.getTuples().get(0).getField(0);
		assertTrue("Result of sum aggregation is 36", agg.getValue() == 36);
	}
	@Test
	public void testGroupByQ() {
		Query q = new Query("SELECT a1, SUM(a2) FROM A GROUP BY a1");
		Relation r = q.execute();
		assertTrue("Tuple size should remain unchanged after aggregating", r.getDesc().getSize() == 8);
		assertTrue("Should be 4 groups from this query",r.getTuples().size() == 4);
		ArrayList<Integer> groups = new ArrayList<Integer>();
		ArrayList<Integer> sums = new ArrayList<Integer>();
		
		for(Tuple t : r.getTuples()) {
			groups.add(((IntField)t.getField(0)).getValue());
			sums.add(((IntField)t.getField(1)).getValue());
		}
		
		assertTrue("Missing grouping", groups.contains(1));
		assertTrue("Missing grouping", groups.contains(530));
		assertTrue("Missing grouping", groups.contains(2));
		assertTrue("Missing grouping", groups.contains(3));
		
		assertTrue("Missing sum", sums.contains(2));
		assertTrue("Missing sum", sums.contains(20));
		assertTrue("Missing sum", sums.contains(6));
		assertTrue("Missing sum", sums.contains(8));
	}
	@Test
	public void testCountAggregationQ() {
		Query q = new Query("SELECT COUNT(*) FROM A");
		Relation r = q.execute();
		assertTrue("The count is just one number", r.getTuples().size() == 1);
		
		IntField agg = (IntField) r.getTuples().get(0).getField(0);
		assertTrue("The number of tuples should be 8", agg.getValue() == 8);
	}
	@Test
	public void testProjectQ() {
		Query q = new Query("SELECT C1 AS c5, SUM(a2) FROM A JOIN test ON a.a1 = test.c1");
		Relation r = q.execute();
		
		assertTrue("Projection should remove a column", r.getDesc().getSize() == 8);
		assertTrue("Projection should not remove tuples", r.getTuples().size() == 1);
		assertTrue("Projection removed the wrong column", r.getDesc().getFieldName(0).equals("c5"));
	}
	
}
