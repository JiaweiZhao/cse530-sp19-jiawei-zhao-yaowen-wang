package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import hw1.Field;
import hw1.IntField;
import hw1.RelationalOperator;
import hw3.*;

public class YourHW3Tests {

	@Test
//	public void test() {
//		fail("Not yet implemented");
//	}
	public void testCreateNewRoot() {
		//instantiate a tree with pInner = 3, pLeaf = 2 
		int pInner = 3;
		int pLeaf = 2;
		BPlusTree bt = new BPlusTree(pInner, pLeaf);
		bt.insert(new Entry(new IntField(9), 0));
		bt.insert(new Entry(new IntField(4), 0));
		//                [4,9]
		Node c1_root_old = bt.getRoot();
		//this node is both root and leaf node
		assertTrue(c1_root_old.isLeafNode() == true);
		//after insert 12, the old root will run out of space, so a new root will be created, and 9 will be pushed up into it.
		bt.insert(new Entry(new IntField(12), 0));
		//                [9, ]
		//               /     \
		//            [4,9]   [12, ]
		Node c1_root_new = bt.getRoot();
		//now the root is an inner node
		assertTrue(c1_root_new.isLeafNode() == false);
		InnerNode c1_root_new_inner = (InnerNode)c1_root_new;
		ArrayList<Field> inner_key = c1_root_new_inner.getKeys();
		assertTrue(inner_key.size() == 1);
		assertTrue(inner_key.get(0).compare(RelationalOperator.EQ, new IntField(9)));
		//get children
		ArrayList<Node> root_new_c = c1_root_new_inner.getChildren();
		assertTrue(root_new_c.size() == 2);
		//the new root's left child is a leaf node and contains 4 and 9
		Node root_c_l = root_new_c.get(0);
		assertTrue(root_c_l.isLeafNode() == true);
		LeafNode root_c_ll = (LeafNode)root_c_l;
		ArrayList<Entry> root_c_ll_e = root_c_ll.getEntries();
		assertTrue(root_c_ll_e.size() == 2);
		assertTrue(root_c_ll_e.get(0).getField().compare(RelationalOperator.EQ, new IntField(4)));
		assertTrue(root_c_ll_e.get(1).getField().compare(RelationalOperator.EQ, new IntField(9)));
		//right only contain 12
		Node root_c_r = root_new_c.get(1);
		assertTrue(root_c_r.isLeafNode() == true);
		LeafNode root_c_rl = (LeafNode)root_c_r;
		ArrayList<Entry> root_c_rl_e = root_c_rl.getEntries();
		assertTrue(root_c_rl_e.size() == 1);
		assertTrue(root_c_rl_e.get(0).getField().compare(RelationalOperator.EQ, new IntField(12)));
	}
	@Test
	public void testRemoveLevel() {
		//instantiate a tree with pInner = 3, pLeaf = 2
		int pInner = 3;
		int pLeaf = 2;
		BPlusTree bt = new BPlusTree(pInner, pLeaf);
		bt.insert(new Entry(new IntField(9), 0));
		bt.insert(new Entry(new IntField(4), 0));
		bt.insert(new Entry(new IntField(12), 0));
		bt.insert(new Entry(new IntField(7), 0));
		bt.insert(new Entry(new IntField(2), 0));
		//							[7, ]
		// 						   /     \
		//                        /       \
		//                     [4, ]     [9, ]
		//                     /   \     /   \
		//                 [2,4] [7, ] [9, ] [12, ]    
		//delete 9
		//                     [4,7]
		//                    /  |  \
		//                   /   |   \
		//               [2,4] [7, ] [12, ]
		bt.delete(new Entry(new IntField(9), 0));
		Node root = bt.getRoot();
		assertTrue(root.isLeafNode() == false);
		ArrayList<Field> root_keys = ((InnerNode)root).getKeys();
		assertTrue(root_keys.size() == 2);
		assertTrue(root_keys.get(0).compare(RelationalOperator.EQ, new IntField(4)));
		assertTrue(root_keys.get(1).compare(RelationalOperator.EQ, new IntField(7)));
		ArrayList<Node> root_c = ((InnerNode)root).getChildren();
		assertTrue(root_c.size() == 3);
		Node l = root_c.get(0);
		Node m = root_c.get(1);
		Node r = root_c.get(2);
		assertTrue(l.isLeafNode() == true);
		assertTrue(m.isLeafNode() == true);
		assertTrue(r.isLeafNode() == true);
		ArrayList<Entry> le = ((LeafNode)l).getEntries();
		ArrayList<Entry> me = ((LeafNode)m).getEntries();
		ArrayList<Entry> re = ((LeafNode)r).getEntries();
		assertTrue(le.size() == 2);
		assertTrue(le.get(0).getField().compare(RelationalOperator.EQ, new IntField(2)));
		assertTrue(le.get(1).getField().compare(RelationalOperator.EQ, new IntField(4)));
		assertTrue(me.size() == 1);
		assertTrue(me.get(0).getField().compare(RelationalOperator.EQ, new IntField(7)));
		assertTrue(re.size() == 1);
		assertTrue(re.get(0).getField().compare(RelationalOperator.EQ, new IntField(12)));
	}
	@Test
	public void testPushTrough() {
		//instantiate a tree with pInner = 3, pLeaf = 2
		int pInner = 3;
		int pLeaf = 2;
		BPlusTree bt = new BPlusTree(pInner, pLeaf);
		bt.insert(new Entry(new IntField(9), 0));
		bt.insert(new Entry(new IntField(4), 0));
		bt.insert(new Entry(new IntField(12), 0));
		bt.insert(new Entry(new IntField(7), 0));
		bt.insert(new Entry(new IntField(2), 0));
		bt.insert(new Entry(new IntField(6), 0));
		bt.insert(new Entry(new IntField(1), 0));
		bt.insert(new Entry(new IntField(3), 0));
		//                           [7, ]
		//                         /       \
		//                       /           \
		//                     /               \
		//                 [2,4]               [9, ]
		//                /  |  \             /     \
		//               /   |   \           /       \
		//           [1,2] [3,4] [6,7]     [9, ]    [12, ]
		//delete 12
		bt.delete(new Entry(new IntField(12), 0));
		//                           [4, ]
		//                         /       \
		//                       /           \
		//                     /               \
		//                 [2, ]               [7, ]
		//                /     \             /     \
		//               /       \           /       \
		//           [1,2]       [3,4]     [6,7]    [9, ]
		assertTrue(bt.getRoot().isLeafNode() == false);
		InnerNode root = (InnerNode)bt.getRoot();
		assertTrue(root.getKeys().get(0).compare(RelationalOperator.EQ, new IntField(4)));
		
		ArrayList<Node> root_c = root.getChildren();
		assertTrue(root_c.get(0).isLeafNode() == false);
		assertTrue(root_c.get(1).isLeafNode() == false);
		InnerNode l = (InnerNode)root_c.get(0);
		InnerNode r = (InnerNode)root_c.get(1);
		assertTrue(l.getKeys().get(0).compare(RelationalOperator.EQ, new IntField(2)));
		assertTrue(r.getKeys().get(0).compare(RelationalOperator.EQ, new IntField(7)));
		
		ArrayList<Node> lc = l.getChildren();
		assertTrue(lc.get(0).isLeafNode() == true);
		assertTrue(lc.get(1).isLeafNode() == true);
		ArrayList<Node> rc = r.getChildren();
		assertTrue(rc.get(0).isLeafNode() == true);
		assertTrue(rc.get(1).isLeafNode() == true);
		LeafNode ll = (LeafNode)lc.get(0);
		LeafNode lr = (LeafNode)lc.get(1);
		LeafNode rl = (LeafNode)rc.get(0);
		LeafNode rr = (LeafNode)rc.get(1);
		assertTrue(ll.getEntries().get(0).getField().compare(RelationalOperator.EQ, new IntField(1)));
		assertTrue(ll.getEntries().get(1).getField().compare(RelationalOperator.EQ, new IntField(2)));
		assertTrue(lr.getEntries().get(0).getField().compare(RelationalOperator.EQ, new IntField(3)));
		assertTrue(lr.getEntries().get(1).getField().compare(RelationalOperator.EQ, new IntField(4)));
		assertTrue(rl.getEntries().get(0).getField().compare(RelationalOperator.EQ, new IntField(6)));
		assertTrue(rl.getEntries().get(1).getField().compare(RelationalOperator.EQ, new IntField(7)));
		assertTrue(rr.getEntries().get(0).getField().compare(RelationalOperator.EQ, new IntField(9)));
	}

}
