package test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import hw1.Catalog;
import hw1.Database;
import hw1.HeapFile;
import hw1.HeapPage;
import hw1.IntField;
import hw1.StringField;
import hw1.Tuple;
import hw1.TupleDesc;
import hw4.BufferPool;
import hw4.Permissions;

public class YourHW4Tests {
	
	private Catalog c;
	private BufferPool bp;
	private HeapFile hf;
	private TupleDesc td;
	private int tid;
	
	@Before
	public void setup() {
		
		try {
			Files.copy(new File("testfiles/test.dat.bak").toPath(), new File("testfiles/test.dat").toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.out.println("unable to copy files");
			e.printStackTrace();
		}
		
		c = Database.getCatalog();
		c.loadSchema("testfiles/test.txt");
		
		int tableId = c.getTableId("test");
		td = c.getTupleDesc(tableId);
		hf = c.getDbFile(tableId);
		
		bp = Database.getBufferPool();
		
		tid = c.getTableId("test");
	}
//	@Test
//	public void testMultipleReadLocksAndUpgrade() throws Exception {
//		// fill 40 transactions with read locks on the same page in the buffer pool.
//
//		for(int i = 0; i < 40; i++) {
//			bp.getPage(i, tid, 0, Permissions.READ_ONLY);
//		}
//		// if it reaches here, it means each transaction gets a read lock. 
//		assertTrue(true);
//		// abort the transactions with id being from 1 to 39.
//		for(int i = 1; i < 40; i++) {
//			bp.transactionComplete(i, false);
//		}
//		// Since only transaction 0 is left, we can try upgrading the lock.
//		bp.getPage(0, tid, 0, Permissions.READ_WRITE);
//		// if it reaches here, it means transaction 0 get the write lock.
//		assertTrue(true);	
//	}
//	
//	
	@Test
	public void testInsert() throws Exception {

		Tuple t = new Tuple(td);
		t.setField(0, new IntField(1));
		t.setField(1, new StringField("530"));	
		HeapPage page1 = bp.getPage(0, tid, 0, Permissions.READ_WRITE);
		for(Iterator<Tuple> ti = page1.iterator(); ti.hasNext(); ) {
			Tuple tuple = ti.next();
			System.out.println(tuple.toString());
		}
		// insert the tuple and finish the transaction.
		bp.insertTuple(0, tid, t); 
		bp.transactionComplete(0, true);
		// retrieve the page and search the tuple.
		Database.resetBufferPool(BufferPool.DEFAULT_PAGES);
		HeapPage page = bp.getPage(1, tid, 0, Permissions.READ_ONLY);
		// check field
		boolean succ = false;
		for(Iterator<Tuple> ti = page.iterator(); ti.hasNext(); ) {
			Tuple tuple = ti.next();
			System.out.println(tuple.toString());
			if(tuple.getField(1).toString().equals("530") && tuple.getField(0).toString().equals("1"));
				succ = true;
		}
		assertTrue("No added tuple was found", succ);
		Database.resetBufferPool(BufferPool.DEFAULT_PAGES);
		page = bp.getPage(1, tid, 0, Permissions.READ_WRITE);
		page.deleteTuple(t);
		bp.transactionComplete(1, true);
		page = bp.getPage(1, tid, 0, Permissions.READ_ONLY);
		for(Iterator<Tuple> ti = page1.iterator(); ti.hasNext(); ) {
			Tuple tuple = ti.next();
			System.out.println(tuple.toString());
		}

	}


}
