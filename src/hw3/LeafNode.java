package hw3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import hw1.Field;
import hw1.RelationalOperator;
import hw1.StringField;

public class LeafNode implements Node {
	
	//instance variables
	private int degree;
	private ArrayList<Entry> entries;
	private InnerNode parent;
	private LeafNode left;
	private LeafNode right;
	
	public LeafNode(int degree) {
		//your code here
		
		parent = null;
		this.degree = degree;
		entries = new ArrayList<Entry>();
		left = null;
		right = null;
	}
	
	public ArrayList<Entry> getEntries() {
		//your code here
		//return null;
		return entries;
	}
	
	public void setEntries(ArrayList<Entry> al) {
		this.entries = al;
	}
	
//	public ArrayList<Field> getKeys(){
//		ArrayList<Field> al = new ArrayList<Field>();
//		for (Entry e : entries) {
//			al.add(e.getField());
//		}
//		return al;
//	}
	
	public int getNumOfEntries() {
		return entries.size();
	}

	public int getDegree() {
		//your code here
		//return 0;
		return this.degree;
	}
	
	public boolean isLeafNode() {
		return true;
	}
	
	//helper methods
	public boolean isRootNode() {
		if (parent != null) {
			return false;
		}else {
			return true;
		}
	}
	
	public boolean hasKey(Field f) {
		for (int i = 0; i < this.getNumOfEntries(); i++) {
			if (f.compare(RelationalOperator.EQ, entries.get(i).getField())) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isOverflow() {
		if (this.getNumOfEntries() <= this.getDegree()) {
			return false;
		}else {
			return true;
		}
	}
	
	public void addEntry(Entry e) {
		if (this.getNumOfEntries() == 0) {
			this.getEntries().add(e);
		} else {
			if (e.getField().compare(RelationalOperator.LT, entries.get(0).getField())) {
				entries.add(0, e);
			} else if (e.getField().compare(RelationalOperator.GT, entries.get(entries.size() - 1).getField())) {
				entries.add(e);
			} else {
				for (int i = 0; i < entries.size() - 1; i++) {
					if (e.getField().compare(RelationalOperator.GT, entries.get(i).getField()) && e.getField().compare(RelationalOperator.LT, entries.get(i+1).getField())) {
						entries.add(i+1, e);
					}
				}
			}
		}
	}
	
	public InnerNode getLeafNodeParent() {
		return this.parent;
	}
	
	public void setLeafNodeParent(InnerNode n) {
		parent = n;
	}
	
	public int getEntryIndex(Entry e) {
		return entries.indexOf(e);
	}
	
	public Entry getMaxEntry() {
		return entries.get(entries.size() - 1);
	}
	
	public Entry getMinEntry() {
		return entries.get(0);
	}
	
	public void removeEntry(Entry e) {
		for (int i = 0; i < entries.size(); i++) {
			if (e.getField().compare(RelationalOperator.EQ, entries.get(i).getField()) && e.getPage() == entries.get(i).getPage()) {
				entries.remove(i);
				return;
			}
		}
	}
	
	public boolean canSpareEntry() {
		if (degree % 2 == 0) {
			if (entries.size() >= degree / 2 + 1) {
				return true;
			} else {
				return false;
			}
		} else {
			if (entries.size() >= degree / 2 + 2) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	public boolean isUnderflow() {
		if(degree % 2 == 0) {
			if (entries.size() >= degree / 2) {
				return false;
			} else {
				return true;
			}
		} else {
			if (entries.size() >= degree / 2 + 1) {
				return false;
			} else {
				return true;
			}
		}
		
	}
	
	public void borrowEntry() {
		//borrwo from left
		if (this.getLeftLeafNode() != null) {
			if (this.getLeftLeafNode().canSpareEntry()) {
				LeafNode left = this.getLeftLeafNode();
				
				this.addEntry(new Entry(left.getMaxEntry().getField(), left.getMaxEntry().getPage()));
				left.removeEntry(left.getMaxEntry());
				//fix parent key in parent node
				int index = left.getLeafNodeParent().getChildren().indexOf(left);
//				System.out.println("index is " + index);
				left.getLeafNodeParent().getKeys().set(index, left.getMaxEntry().getField());
//				for (int i = 0; i < this.getLeafNodeParent().getNumOfKeys(); i++) {
//					System.out.println(this.getLeafNodeParent().getKeys().get(i).toString());
//				}
				return;
			} 
		} 
		//System.out.println("didnt borrow left");
		//borrow from right
		if (this.getRightLeafNode() != null) {
			if (this.getRightLeafNode().canSpareEntry()) {
				LeafNode right = this.getRightLeafNode();
				this.addEntry(new Entry(right.getMinEntry().getField(), right.getMinEntry().getPage()));
				right.removeEntry(right.getMinEntry());
				int index = this.getLeafNodeParent().getChildren().indexOf(this);
				//System.out.println("index is " + index);
				this.getLeafNodeParent().getKeys().set(index, this.getMaxEntry().getField());
//				for (int i = 0; i < this.getLeafNodeParent().getNumOfKeys(); i++) {
//					System.out.println(this.getLeafNodeParent().getKeys().get(i).toString());
//				}
				return;
			}
		}
		//System.out.println("didnt borrow right");
		//try merge left
		if (this.getLeftLeafNode() != null) {
			this.mergeLeftLeafNode();
		} else {
			this.mergeRightLeafNode();
		}

	}
	
	public void mergeLeftLeafNode() {
		LeafNode left = this.getLeftLeafNode();
		LeafNode right = this.getRightLeafNode();
		//move all the entries from this node to the left node
		//System.out.println("this is " + this.getEntries().get(0).getField().toString());
		for (int i = 0; i < this.getNumOfEntries(); i++) {
			left.addEntry(new Entry(this.getEntries().get(i).getField(), this.getEntries().get(i).getPage()));
		}
		//fix the left and right pointer
		if (right != null) {
			right.setLeftLeafNode(left);
		}
		
		left.setRightLeafNode(right);
		
		//fix parent key
		
		int this_index = this.getLeafNodeParent().getChildren().indexOf(this);
		int left_index = left.getLeafNodeParent().getChildren().indexOf(left);
		//this does not have a key in parent
		if(this_index >= this.getLeafNodeParent().getNumOfKeys()) {
			
			if (left_index >= left.getLeafNodeParent().getNumOfKeys()) {
				System.out.println("not possible to print this message");
			} else {
				//left.getLeafNodeParent().getKeys().remove(left_index);
				left.getLeafNodeParent().getKeys().set(left_index, left.getMaxEntry().getField());
			}
		//this has a key in parent
		} else {
			//System.out.println(this_index);
			this.getLeafNodeParent().getKeys().remove(this_index);
			
			if (left_index >= left.getLeafNodeParent().getNumOfKeys()) {
				
			} else {
				left.getLeafNodeParent().getKeys().set(left_index, left.getMaxEntry().getField());
			}
		}
		this.getLeafNodeParent().getChildren().remove(this_index);
		
		if (left.getLeafNodeParent().isRootNode()) {
			if (left_index >= left.getLeafNodeParent().getNumOfKeys()) {
				left.getLeafNodeParent().getKeys().set(left.getLeafNodeParent().getChildren().indexOf(left), left.getMaxEntry().getField());
			}
		} else {
			int i = left.getLeafNodeParent().getInnerNodeParent().getChildren().indexOf(left.getLeafNodeParent());
			if (i >= left.getLeafNodeParent().getInnerNodeParent().getNumOfKeys()) {
				
			} else {
				if (left.getLeafNodeParent().getInnerNodeParent().getKeys().get(i).compare(RelationalOperator.LT, left.getMaxEntry().getField())) {
					left.getLeafNodeParent().getInnerNodeParent().getKeys().set(i, left.getMaxEntry().getField());
				}
			}

			
			//System.out.println("max is " + left.getMaxEntry().getField());
		}

//		int this_index = this.getLeafNodeParent().getChildren().indexOf(this);
//		int left_index = left.getLeafNodeParent().getChildren().indexOf(left);
//		if (left_index < left.getLeafNodeParent().getNumOfKeys()) {
//			left.getLeafNodeParent().getKeys().set(left_index, left.getMaxEntry().getField());
//		} 
//		Field max = this.getMaxEntry().getField();
//		this.getLeafNodeParent().getChildren().remove(this);
//		left.getLeafNodeParent().getKeys().set(index, max);
//		this.getLeafNodeParent().getKeys().remove(index);
		//might need fix
	}
	
	public void mergeRightLeafNode() {
		LeafNode left = this.getLeftLeafNode();
		LeafNode right = this.getRightLeafNode();
		
		for (int i = 0; i < this.getNumOfEntries(); i++) {
			right.addEntry(new Entry(this.getEntries().get(i).getField(), this.getEntries().get(i).getPage()));
		}
		if (left != null) {
			left.setRightLeafNode(right);
		}
		
		right.setLeftLeafNode(left);
		
		//int index = this.getLeafNodeParent().getChildren().indexOf(this);
		//might need fix
		
		
		int index = this.getLeafNodeParent().getChildren().indexOf(this);
		this.getLeafNodeParent().getChildren().remove(index);
		this.getLeafNodeParent().getKeys().remove(index);
		//System.out.println(right.getLeftLeafNode() == null);
//		for (int i = 0; i < right.getNumOfEntries(); i++) {
//			System.out.println("right: " + right.getEntries().get(i).getField().toString());
//		}
//		for (int i = 0; i < right.getRightLeafNode().getNumOfEntries(); i++) {
//			System.out.println("right.right: " + right.getRightLeafNode().getEntries().get(i).getField().toString());
//		}
//		for (int i = 0; i < right.getRightLeafNode().getRightLeafNode().getNumOfEntries(); i++) {
//			System.out.println("right.right.right: " + right.getRightLeafNode().getRightLeafNode().getEntries().get(i).getField().toString());
//		}
	}

	public LeafNode getLeftLeafNode() {
		return this.left;
	}
	
	public LeafNode getRightLeafNode() {
		return this.right;
	}

	public void setLeftLeafNode(LeafNode ln) {
		this.left = ln;
	}

	public void setRightLeafNode(LeafNode ln) {
		this.right = ln;
	}

}