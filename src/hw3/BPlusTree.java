package hw3;


import java.util.ArrayList;

import hw1.Field;
import hw1.IntField;
import hw1.RelationalOperator;

public class BPlusTree {
	static int pInner;
	static int pLeaf;
	static String keyDataType;
    private Node root;
    private InnerNode parent;
    public BPlusTree(int pInner, int pLeaf) {
    	//your code here
    	root = new LeafNode(pLeaf);
    	//current_node = new LeafNode(pLeaf);
    	parent = null;
    	this.pInner = pInner;
    	this.pLeaf = pLeaf;
    }
    
    public LeafNode search(Field f) {
    	//your code here
    	Node current_node = root;
    	//System.out.println("searching for " + f.toString());
    	while (!current_node.isLeafNode()) {
    		InnerNode in = this.castToInnerNode(current_node);
        	current_node = in.findNextChild(f);
    	}
    	LeafNode current_leaf_node = this.castToLeafNode(current_node);
    	if (current_leaf_node.hasKey(f)) {
    		return current_leaf_node;
    	}else {
    		return null;
    	}
    }
    
    public void insert(Entry e) {
    	//your code here
    	LeafNode ln = this.search(e.getField());
    	if (ln == null) {
    		insertHelper(root, e);
    	}
    }
    
    public void insertHelper(Node n, Entry e) {
    	if(n.isLeafNode()) {
    		LeafNode ln = this.castToLeafNode(n);
    		ln.addEntry(e);
    		if (ln.isOverflow()) {
    			this.splitHelper(ln);
    		}
    	} else {
    		InnerNode in = this.castToInnerNode(n);
    		insertHelper(in.findNextChild(e.getField()), e);
    		if (in.isOverflow()) {
    			this.splitHelper(in);
    		}
    	}
    }
    
    public void splitHelper(Node n) {
    	if (n.isLeafNode()) {
    		LeafNode rightln = this.castToLeafNode(n);
        	LeafNode leftln = new LeafNode(pLeaf);
        	LeafNode newln = new LeafNode(pLeaf);
    		ArrayList<Entry> el = new ArrayList<>();
    		ArrayList<Entry> er = new ArrayList<>();
    		for (int i = 0; i < rightln.getNumOfEntries(); i++) {
    			if (rightln.getNumOfEntries() % 2 == 0) {
        			if (i < rightln.getNumOfEntries() / 2) {
        				el.add(rightln.getEntries().get(i));
        			} else {
        				er.add(rightln.getEntries().get(i));
        			}
    			}else {
    				if (i <= rightln.getNumOfEntries() / 2) {
        				el.add(rightln.getEntries().get(i));
        			} else {
        				er.add(rightln.getEntries().get(i));
        			}
    			}
    		}
    		Field pushUp = el.get(el.size() - 1).getField();
    		//System.out.println("the push up value from leafnode is " + pushUp.toString());
    		rightln.setEntries(er);
    		leftln.setEntries(el);
    		if (rightln.getLeafNodeParent() == null) {
    			InnerNode p = new InnerNode(pInner - 1);
    			root = p;
    			p.addKey(pushUp);
    			p.addChild(rightln);
    			p.addChild(leftln);
//    			int old_index = p.getChildren().indexOf(rightln);
//    			
//    			this.castToLeafNode(p.getChildren().get(old_index)).setLeftLeafNode(this.castToLeafNode(p.getChildren().get(old_index - 1)));
//    			this.castToLeafNode(p.getChildren().get(old_index - 1)).setRightLeafNode(this.castToLeafNode(p.getChildren().get(old_index)));
    		} else {
    			InnerNode p = rightln.getLeafNodeParent();
    			p.addKey(pushUp);
    			p.addChild(leftln);
//    			int old_index = p.getChildren().indexOf(rightln);
//    			System.out.println("old index is " + old_index);
//    			this.castToLeafNode(p.getChildren().get(old_index - 1)).setLeftLeafNode(this.castToLeafNode(p.getChildren().get(old_index)).getLeftLeafNode());
//    			this.castToLeafNode(p.getChildren().get(old_index - 1)).setRightLeafNode(this.castToLeafNode(p.getChildren().get(old_index)));
//    			this.castToLeafNode(p.getChildren().get(old_index)).setLeftLeafNode(this.castToLeafNode(p.getChildren().get(old_index - 1)));
    			LeafNode l = ((LeafNode) p.getChildren().get(0));
    		}
    	} else {
    		InnerNode rightin = this.castToInnerNode(n);
//    		for (int i = 0 ; i < rightin.getNumOfKeys(); i ++) {
//    			System.out.println(rightin.getKeys().get(i).toString());
//    		}
        	InnerNode leftin = new InnerNode(pInner - 1);
    		ArrayList<Node> nl = new ArrayList<>();
    		ArrayList<Node> nr = new ArrayList<>();
    		ArrayList<Field> fl = new ArrayList<>();
    		ArrayList<Field> fr = new ArrayList<>();
    		
    		for (int i = 0; i < rightin.getNumOfKeys(); i++) {
    			if (rightin.getNumOfKeys() % 2 == 0) {
    				if (i < rightin.getNumOfKeys() / 2 - 1) {
    					fl.add(rightin.getKeys().get(i));
    					//System.out.println("fl has " + rightin.getKeys().get(i).toString());
    				} else if (i > rightin.getNumOfKeys() / 2 - 1) {
    					fr.add(rightin.getKeys().get(i));
    					//System.out.println("fr has " + rightin.getKeys().get(i).toString());
    				}
    			} else {
    				if (i < rightin.getNumOfKeys() / 2) {
        				fl.add(rightin.getKeys().get(i));
        				//System.out.println("fl has " + rightin.getKeys().get(i).toString());
        			} else if (i > rightin.getNumOfKeys() / 2){
        				fr.add(rightin.getKeys().get(i));
        				//System.out.println("fr has " + rightin.getKeys().get(i).toString());
        			}
    			}
    		}
    		Field pushUp;
    		if (rightin.getNumOfKeys() % 2 == 0) {
    			pushUp = rightin.getKeys().get(rightin.getNumOfKeys() / 2 - 1);
    		} else {
    			pushUp = rightin.getKeys().get(rightin.getNumOfKeys() / 2);
    		}
    		
    		//System.out.println("the push up value from innernode is " + pushUp.toString());
    		rightin.setKeys(fr);
    		leftin.setKeys(fl);
    		for(int i = 0; i < rightin.getNumOfChildren(); i++) {
    			if (i < rightin.getNumOfChildren() / 2) {
    				nl.add(rightin.getChildren().get(i));
    			} else {
    				nr.add(rightin.getChildren().get(i));
    			}
    		}
    		rightin.setChildren(nr);
    		leftin.setChildren(nl);
    		if (rightin.getInnerNodeParent() == null) {
    			InnerNode p = new InnerNode(pInner - 1);
    			root = p;
    			p.addKey(pushUp);
    			p.addChild(rightin);
    			p.addChild(leftin);
    		} else {
    			InnerNode p = rightin.getInnerNodeParent();
    			p.addKey(pushUp);
    			p.addChild(leftin);
    		}
    	}
    }

    
    
    public void delete(Entry e) {
    	//your code here
    	LeafNode ln = this.search(e.getField());
    	if (ln == null) {
    		System.out.println("key " + e.getField().toString() + " is not found");
    	} else {
    		deleteHelper(root, e);
    	}
    }
    
    public void deleteHelper(Node n, Entry e) {
    	if(n.isLeafNode()) {
    		LeafNode ln = this.castToLeafNode(n);
    		
    		ln.removeEntry(e);
    		
    		if (ln.isRootNode()) {
    			//is root node, can can be less than half full
    		} else {
    			if (ln.isUnderflow()) {
        			ln.borrowEntry();
        		}
    		}
    	} else {
    		InnerNode in = this.castToInnerNode(n);
    		deleteHelper(in.findNextChild(e.getField()), e);
    		if (in.isInvalidNode()) {
    			System.out.println("this node is invalid!");
    			if (in.isRootNode()) {
    				if (in.getNumOfChildren() == 1) {
    					root = in.getChildren().get(0);
    					if (in.getChildren().get(0).isLeafNode()) {
    						((LeafNode)in.getChildren().get(0)).setLeafNodeParent(null);
    					} else {
    						((InnerNode)in.getChildren().get(0)).setInnerNodeParent(null);
    					}
    					
    				}
    			} else {
    				in.pushThrough();
    			}
    			
    		}
    	}
    }
    
    public Node getRoot() {
    	//your code here
    	//return null;
    	return root;
    }

    public LeafNode castToLeafNode(Node n) {
    	return (LeafNode)n;
    }
    
    public InnerNode castToInnerNode(Node n) {
    	return (InnerNode)n;
    }
    


	
}
