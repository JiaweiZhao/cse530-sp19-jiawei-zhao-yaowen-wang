package hw3;

import java.util.ArrayList;
import java.util.Arrays;

import hw1.Field;
import hw1.IntField;
import hw1.RelationalOperator;

public class InnerNode implements Node {
	
	//instance variables
	private int degree;
	private ArrayList<Field> keys;
	private ArrayList<Node> children;
	private InnerNode parent;
	
	public InnerNode(int degree) {
		//your code here
		this.degree = degree;
		keys = new ArrayList<Field>();
		children = new ArrayList<Node>();
		parent = null;
	}
	
	public ArrayList<Field> getKeys() {
		//your code here
		//return null;
		return keys;
	}
	
	public ArrayList<Node> getChildren() {
		//your code here
		//return null;
		return children;
	}
	
	public void setKeys(ArrayList<Field> keys) {
		this.keys = keys;
	}

	public void setChildren(ArrayList<Node> children) {
		this.children = children;
		for (int i = 0; i < children.size();i++) {
			if(children.get(i).isLeafNode()) {
				((LeafNode)children.get(i)).setLeafNodeParent(this);
			}else {
				((InnerNode)children.get(i)).setInnerNodeParent(this);;
			}
		}
	}
	
	public void addChild(Node n) {
		if (n.isLeafNode()) {
			LeafNode ln = (LeafNode) n;
			if (children.size() == 0) {
				children.add(ln);
				ln.setLeftLeafNode(null);
				ln.setRightLeafNode(null);
			} else {
				if (ln.getMaxEntry().getField().compare(RelationalOperator.LTE, keys.get(0))) {
					children.add(0, ln);
					int index = children.indexOf(ln);
					((LeafNode)children.get(index)).setLeftLeafNode(null);
					((LeafNode)children.get(index)).setRightLeafNode((LeafNode)children.get(index + 1));
					((LeafNode)children.get(index + 1)).setLeftLeafNode((LeafNode)children.get(index));
				} else if (ln.getMinEntry().getField().compare(RelationalOperator.GT, keys.get(keys.size() - 1))) {
					children.add(ln);
					int index = children.indexOf(ln);
					((LeafNode)children.get(index)).setRightLeafNode(null);
					((LeafNode)children.get(index)).setLeftLeafNode((LeafNode)children.get(index - 1));
					((LeafNode)children.get(index - 1)).setRightLeafNode((LeafNode)children.get(index));
				} else {
					for (int i = 0; i < keys.size() - 1; i++) {
						if (ln.getMinEntry().getField().compare(RelationalOperator.GT, keys.get(i)) && ln.getMaxEntry().getField().compare(RelationalOperator.LTE, keys.get(i+1))) {
							children.add(i+1, ln);
							ln.setLeftLeafNode((LeafNode)children.get(i));
							ln.setRightLeafNode((LeafNode)children.get(i+2));
							((LeafNode)children.get(i)).setRightLeafNode((LeafNode)children.get(i+1));
							((LeafNode)children.get(i+2)).setLeftLeafNode((LeafNode)children.get(i+1));
						}
					}
				}
				
			}
			ln.setLeafNodeParent(this);
		} else {
			InnerNode in = (InnerNode) n;
			if (children.size() == 0) {
				children.add(in);
			}else {
				if (in.getMaxField().compare(RelationalOperator.LTE, keys.get(0))) {
					children.add(0, in);
				} else if (in.getMinField().compare(RelationalOperator.GT, keys.get(keys.size() - 1))) {
					children.add(in);
				} else {
					for (int i = 0; i < keys.size() - 1; i++) {
						if (in.getMinField().compare(RelationalOperator.GT, keys.get(0)) && in.getMaxField().compare(RelationalOperator.LTE, keys.get(i+1))) {
							children.add(i+1, in);
						}
					}
				}
			}
			in.setInnerNodeParent(this);
		}
	}
	
	public void sortChildren() {
		
	}

	
	public int getDegree() {
		//your code here
		//return 0;
		return this.degree;
	}
	
	public boolean isLeafNode() {
		return false;
	}

	//helper methods
	

	public Field getMaxField() {
		for (int i = keys.size() - 1; i >= 0; i--) {
			if (keys.get(i) != null) {
				return keys.get(i);
			}
		}
		return null;
	}
	
	public Field getMinField() {
		if (keys.get(0) != null) {
			return keys.get(0);
		}
		return null;
	}
	
	public boolean isRootNode() {
		if (parent != null) {
			return false;
		}else {
			return true;
		}
	}
	
	public int getNumOfKeys() {
		return keys.size();
	}
	
	public boolean noGapsInKeys() {
		for (int i = 0; i < keys.size() - 1; i++){
			if (keys.get(i) == null && keys.get(1+1) != null){
				return false;
			}
		}
	    return true;
	}
	
    public Node findNextChild(Field f) {
    	for(int i = 0; i < keys.size(); i++) {
    		if (f.compare(RelationalOperator.LTE, keys.get(i))) {
    			return children.get(i);
    		}
    	}
    	return children.get(keys.size());

    }
	
	public int getNumOfChildren() {
		return children.size();
	}
	
	public boolean noGapsInChildren() {
		for (int i = 0; i < children.size() - 1; i++){
			if (children.get(i) == null && children.get(i+1) != null){
				return false;
			}
		}
	    return true;
	}
	
	public boolean isOverflow() {
		if(keys.size() <= degree) {
			return false;
		} else {
			return true;
		}
	}
	
	public boolean hasKey(Field f) {
		for (int i = 0; i < this.getNumOfKeys(); i++) {
			if (f.compare(RelationalOperator.EQ, keys.get(i))) {
				return true;
			}
		}
		return false;
	}
	
	public void addKey(Field f) {
		//System.out.println("the key to be added is " + f.toString());
		if (keys.size() == 0) {
			keys.add(f);
		} else {
			if (f.compare(RelationalOperator.LT, keys.get(0))) {
				keys.add(0, f);
			}else if (f.compare(RelationalOperator.GT, keys.get(keys.size() - 1))) {
				keys.add(f);
			} else {
				for (int i = 0; i < keys.size() - 1; i++) {
					if (f.compare(RelationalOperator.GT, keys.get(i)) && f.compare(RelationalOperator.LT, keys.get(i+1))) {
						keys.add(i+1, f);
					}
				}
			}
		}
//		for (int i = 0; i < keys.size(); i++) {
//			System.out.println("after insert key is " + keys.get(i).toString());
//		}

	}
	
	public InnerNode getInnerNodeParent() {
		return this.parent;
	}
	
	public ArrayList<Node> getSiblings(){
		return this.getInnerNodeParent().getChildren();
	}

	public void setInnerNodeParent(InnerNode n) {
		parent = n;
	}
	
	public boolean isInvalidNode() {
		//System.out.println("this innernode has " + this.getNumOfKeys() + "keys and " + this.getNumOfChildren() + "children");
		if (this.getNumOfKeys() + 1 ==  this.getNumOfChildren() && this.getNumOfChildren() >= (this.degree + 1) / 2 && this.getNumOfKeys() > 0) {
			//System.out.println("false");
			return false;
		} else {
			//System.out.println("true");
			return true;
		}
	}
	
	public void pushThrough() {
		int this_index = this.getInnerNodeParent().getChildren().indexOf(this);
		if (this_index == 0) {
			borrowRightChild(this_index);
		} else if (this_index == this.getInnerNodeParent().getNumOfChildren() - 1) {
			boolean flag = borrowLeftChild(this_index);
			if (flag) {
				this.mergeLeftInnerNode();
			}
		} else {
			boolean flag_left = borrowLeftChild(this_index);
			boolean flag_right = true;
			if (flag_left) {
				flag_right = borrowRightChild(this_index);
			}
			if (flag_left == true && flag_right == true) {
				//this.removeLevel();
			}
		}
	}
	
	public void removeLevel() {
		if (this.getInnerNodeParent().getChildren().indexOf(this) == 0) {
			
		} else {
			this.mergeLeftInnerNode();
		}
	}
	
	public void mergeLeftInnerNode() {
		int this_index = this.getInnerNodeParent().getChildren().indexOf(this);
		Field pushDown = this.getInnerNodeParent().getKeys().get(this_index - 1);
		int this_children_length = this.getNumOfChildren();
		for (int i = 0; i < this_children_length;i++) {
			if (this.getChildren().get(0).isLeafNode()) {
				((LeafNode)this.getChildren().get(0)).setLeafNodeParent((InnerNode)this.getInnerNodeParent().getChildren().get(this_index - 1));
			} else {
				((InnerNode)this.getChildren().get(0)).setInnerNodeParent((InnerNode)this.getInnerNodeParent().getChildren().get(this_index - 1));
			}
			((InnerNode)this.getInnerNodeParent().getChildren().get(this_index - 1)).getChildren().add(this.getChildren().get(0));
			this.getChildren().remove(0);
		}
		((InnerNode)this.getInnerNodeParent().getChildren().get(this_index - 1)).getKeys().add(pushDown);
		//delete this child from this parent
		this.getInnerNodeParent().getChildren().remove(this_index);
		//this.getInnerNodeParent().getKeys().remove(this_index - 1);
	}
	
	public boolean borrowLeftChild(int this_index) {
		if (((InnerNode)this.getInnerNodeParent().getChildren().get(this_index - 1)).canSpareChild()) {
			Field pushUp = ((InnerNode)this.getInnerNodeParent().getChildren().get(this_index - 1)).getMaxField();
			int largest_index = ((InnerNode)this.getInnerNodeParent().getChildren().get(this_index - 1)).getNumOfKeys() - 1;
			((InnerNode)this.getInnerNodeParent().getChildren().get(this_index - 1)).getKeys().remove(largest_index);
			if (((InnerNode)this.getInnerNodeParent().getChildren().get(this_index - 1)).getChildren().get(largest_index + 1).isLeafNode()){
				this.getChildren().add(0, (LeafNode)((InnerNode)this.getInnerNodeParent().getChildren().get(this_index - 1)).getChildren().get(largest_index + 1));
				((LeafNode)((InnerNode)this.getInnerNodeParent().getChildren().get(this_index - 1)).getChildren().get(largest_index + 1)).setLeafNodeParent(this);
			}
			((InnerNode)this.getInnerNodeParent().getChildren().get(this_index - 1)).getChildren().remove(largest_index + 1);
			//System.out.println("the push up key is "+ pushUp.toString());
			this.getInnerNodeParent().getKeys().add(this_index - 1, pushUp);
			
			Field pushDown = this.getInnerNodeParent().getKeys().get(this_index);
			this.getKeys().set(0, pushDown);
			this.getInnerNodeParent().getKeys().remove(this_index);
			//System.out.println("the push down key is "+ pushDown.toString());
			return false;
		}
		return true;
	}
	
	public boolean borrowRightChild(int this_index) {
		
		if (((InnerNode)this.getInnerNodeParent().getChildren().get(this_index + 1)).canSpareChild()) {
			Field pushDown = this.getInnerNodeParent().getKeys().get(this_index);
			//System.out.println("the push down key is "+ pushDown.toString());
			//this.addKey(pushDown);
			this.getKeys().set(this.getNumOfKeys() - 1, pushDown);
			Field pushUp = ((InnerNode)this.getInnerNodeParent().getChildren().get(this_index + 1)).getKeys().get(0);
			//System.out.println("the push up key is "+ pushUp.toString());
			this.getInnerNodeParent().getKeys().set(this_index, pushUp);
			((InnerNode)this.getInnerNodeParent().getChildren().get(this_index + 1)).getKeys().remove(0);
			this.getChildren().add(((InnerNode)this.getInnerNodeParent().getChildren().get(this_index + 1)).getChildren().get(0));
			if (((InnerNode)this.getInnerNodeParent().getChildren().get(this_index + 1)).getChildren().get(0).isLeafNode()) {
				((LeafNode)((InnerNode)this.getInnerNodeParent().getChildren().get(this_index + 1)).getChildren().get(0)).setLeafNodeParent(this);
			} else {
				((InnerNode)((InnerNode)this.getInnerNodeParent().getChildren().get(this_index + 1)).getChildren().get(0)).setInnerNodeParent(this);
			}
			
			((InnerNode)this.getInnerNodeParent().getChildren().get(this_index + 1)).getChildren().remove(0);
			return false;
			//this.addChild(n);
		}
		return true;
	}
	
	public boolean canSpareChild() {
		if (degree % 2 == 0) {
			if (this.getNumOfChildren() > (degree + 1) / 2 + 1) {
				return true;
			} else {
				return false;
			}
		} else {
			if (this.getNumOfChildren() > (degree + 1) / 2) {
				return true;
			} else {
				return false;
			}
		}

	}
	
}