//Jiawei Zhao
//Yaowen Wang
package hw1;

import java.util.HashMap;

/**
 * This class represents a tuple that will contain a single row's worth of information
 * from a table. It also includes information about where it is stored
 * @author Sam Madden modified by Doug Shook
 *
 */
public class Tuple {
	//instance variable

	private TupleDesc td;
	private HashMap<String, Field> hm;
	private int Pid;
	private int slotid;
	/**
	 * Creates a new tuple with the given description
	 * @param t the schema for this tuple
	 */
	public Tuple(TupleDesc t) {
		//your code here
		hm = new HashMap<String, Field>();
		td = t;
		for (int i = 0; i < t.numFields(); i++) {
			hm.put(t.getFieldName(i), null);
		}
	}
	
	public TupleDesc getDesc() {
		//your code here
		return td;
	}
	
	/**
	 * retrieves the page id where this tuple is stored
	 * @return the page id of this tuple
	 */
	public int getPid() {
		//your code here
		return this.Pid;
	}

	public void setPid(int pid) {
		//your code here
		this.Pid = pid;
	}

	/**
	 * retrieves the tuple (slot) id of this tuple
	 * @return the slot where this tuple is stored
	 */
	public int getId() {
		//your code here
		return this.slotid;
	}

	public void setId(int id) {
		//your code here
		this.slotid = id;
	}
	
	public void setDesc(TupleDesc td) {
		//your code here;
		this.td = td;
	}
	
	/**
	 * Stores the given data at the i-th field
	 * @param i the field number to store the data
	 * @param v the data
	 */
	public void setField(int i, Field v) {
		//your code here
		hm.put(td.getFieldName(i), v);
	}
	
	public Field getField(int i) {
		//your code here
		return hm.get(td.getFieldName(i));
	}
	
	/**
	 * Creates a string representation of this tuple that displays its contents.
	 * You should convert the binary data into a readable format (i.e. display the ints in base-10 and convert
	 * the String columns to readable text).
	 */
	public String toString() {
		//your code here
		String str = "";
		for (int i = 0; i < this.td.numFields(); i++) {
			str += this.getField(i).getType() + ": " + this.getField(i).toString() + ",";
		}
		str = str.substring(0, str.length() - 1);
		return str;
	}
}
	