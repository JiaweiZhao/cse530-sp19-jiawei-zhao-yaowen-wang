//Jiawei Zhao
//Yaowen Wang
package hw1;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

import net.sf.jsqlparser.util.AddAliasesVisitor;

public class HeapPage {

	private int id;
	private byte[] header;
	private Tuple[] tuples;
	private TupleDesc td;
	private int numSlots;
	private int tableId;

	// the ID of the transction that holds the write lock. -1 means it is available.
	private int writeLock;
	// the set of IDs of transaction that hold the read lock
	private HashSet<Integer> readLocks;
	// if this page is modified, mark it as true;
	public boolean isDirty;
	
	public int getTableId() {
		return this.tableId;
	}

	public HeapPage(int id, byte[] data, int tableId) throws IOException {
		
		this.isDirty = false;
		this.writeLock = -1;
		this.readLocks = new HashSet<>();
		
		this.id = id;
		this.tableId = tableId;

		this.td = Database.getCatalog().getTupleDesc(this.tableId);
		this.numSlots = getNumSlots();
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));

		// allocate and read the header slots of this page
		header = new byte[getHeaderSize()];
		for (int i=0; i<header.length; i++)
			header[i] = dis.readByte();

		try{
			// allocate and read the actual records of this page
			tuples = new Tuple[numSlots];
			for (int i=0; i<tuples.length; i++)
				tuples[i] = readNextTuple(dis,i);
		}catch(NoSuchElementException e){
			e.printStackTrace();
		}
		dis.close();
	}
	
	public void addReadLocks(int tid) {
		this.readLocks.add(tid);
	}
	public void addWriteLock(int tid) {
		this.writeLock = tid;
	}
	public HashSet<Integer> getAllReadLocks(){
		return this.readLocks;
	}
	public int getWriteLock() {
		return this.writeLock;
	}

	public int getId() {
		//your code here
		return this.id;
	}

	public boolean hasOpenSlots() {
		for(int i = 0; i < this.getNumSlots(); i++) {
			if(!this.slotOccupied(i))
				return true;
		}
		return false;
	}
	/**
	 * Computes and returns the total number of slots that are on this page (occupied or not).
	 * Must take the header into account!
	 * @return number of slots on this page
	 */
	public int getNumSlots() {
		//your code here
		int ns = (HeapFile.PAGE_SIZE * 8) / (td.getSize() * 8 + 1); 
		return ns;
	}

	/**
	 * Computes the size of the header. Headers must be a whole number of bytes (no partial bytes)
	 * @return size of header in bytes
	 */
	private int getHeaderSize() {        
		//your code here
		if (this.getNumSlots() % 8 == 0) {
			return this.getNumSlots() / 8;
		} else {
			return (this.getNumSlots() / 8) + 1;
		}
		
	}

	/**
	 * Checks to see if a slot is occupied or not by checking the header
	 * @param s the slot to test
	 * @return true if occupied
	 */
	public boolean slotOccupied(int s) {
		//your code here
		if (((header[s / 8] >> (s % 8)) & 1) == 1) {
			return true;
		}else {
			return false;
		}
	}

	/**
	 * Sets the occupied status of a slot by modifying the header
	 * @param s the slot to modify
	 * @param value its occupied status
	 */
	public void setSlotOccupied(int s, boolean value) {
		//your code here
		if(value ^ this.slotOccupied(s)) {
			if(value) {
				header[s/8] += ((byte)Math.pow(2, s % 8));
			} else {
				header[s/8] -= ((byte)Math.pow(2, s % 8));
			}
		}
	}
	
	/**
	 * Adds the given tuple in the next available slot. Throws an exception if no empty slots are available.
	 * Also throws an exception if the given tuple does not have the same structure as the tuples within the page.
	 * @param t the tuple to be added.
	 * @throws Exception
	 */
	public void addTuple(Tuple t) throws Exception {
		//your code here
		boolean flag = true;
		//System.out.println(t.getDesc().equals(this.td));
		//System.out.println("# of slots = " + this.numSlots);
		if(t.getDesc().equals(this.td)) {
			
			for (int i = 0; i < this.getNumSlots(); i++) {
				
				if (!this.slotOccupied(i)) {
					//add the tuple
					tuples[i] = t;
					//set slotid
					t.setId(i);
					//set header
					this.setSlotOccupied(i, true);
					//add a pid to this tuple
					t.setPid(this.id);
					
					flag = false;
					break;
				}
			}
			if (flag) {
				throw new Exception("no empty slots are available.");
			}
		} else {
			throw new Exception("the given tuple does not have the same structure as the tuples within the page.");
		}
	}

	/**
	 * Removes the given Tuple from the page. If the page id from the tuple does not match this page, throw
	 * an exception. If the tuple slot is already empty, throw an exception
	 * @param t the tuple to be deleted
	 * @throws Exception
	 */
	public void deleteTuple(Tuple t) throws Exception {
		//your code here
		if (t.getPid() == this.id) {
			if (this.slotOccupied(t.getId())) {
					//set slot to false
				this.setSlotOccupied(t.getId(), false);
			} else {
				throw new Exception("the tuple slot is already empty");
			}
		} else {
			throw new Exception("the page id from the tuple does not match this page");
		}
	}
	
	/**
     * Suck up tuples from the source file.
     */
	private Tuple readNextTuple(DataInputStream dis, int slotId) {
		// if associated bit is not set, read forward to the next tuple, and
		// return null.
		if (!slotOccupied(slotId)) {
			for (int i=0; i<td.getSize(); i++) {
				try {
					dis.readByte();
				} catch (IOException e) {
					throw new NoSuchElementException("error reading empty tuple");
				}
			}
			return null;
		}

		// read fields in the tuple
		Tuple t = new Tuple(td);
		t.setPid(this.id);
		t.setId(slotId);

		for (int j=0; j<td.numFields(); j++) {
			if(td.getType(j) == Type.INT) {
				byte[] field = new byte[4];
				try {
					dis.read(field);
					t.setField(j, new IntField(field));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				byte[] field = new byte[129];
				try {
					dis.read(field);
					t.setField(j, new StringField(field));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}


		return t;
	}

	/**
     * Generates a byte array representing the contents of this page.
     * Used to serialize this page to disk.
	 *
     * The invariant here is that it should be possible to pass the byte
     * array generated by getPageData to the HeapPage constructor and
     * have it produce an identical HeapPage object.
     *
     * @return A byte array correspond to the bytes of this page.
     */
	public byte[] getPageData() {
		int len = HeapFile.PAGE_SIZE;
		ByteArrayOutputStream baos = new ByteArrayOutputStream(len);
		DataOutputStream dos = new DataOutputStream(baos);

		// create the header of the page
		for (int i=0; i<header.length; i++) {
			try {
				dos.writeByte(header[i]);
			} catch (IOException e) {
				// this really shouldn't happen
				e.printStackTrace();
			}
		}

		// create the tuples
		for (int i=0; i<tuples.length; i++) {

			// empty slot
			if (!slotOccupied(i)) {
				for (int j=0; j<td.getSize(); j++) {
					try {
						dos.writeByte(0);
					} catch (IOException e) {
						e.printStackTrace();
					}

				}
				continue;
			}

			// non-empty slot
			for (int j=0; j<td.numFields(); j++) {
				Field f = tuples[i].getField(j);
				try {
					dos.write(f.toByteArray());

				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		// padding
		int zerolen = HeapFile.PAGE_SIZE - (header.length + td.getSize() * tuples.length); //- numSlots * td.getSize();
		byte[] zeroes = new byte[zerolen];
		try {
			dos.write(zeroes, 0, zerolen);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			dos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return baos.toByteArray();
	}

	/**
	 * Returns an iterator that can be used to access all tuples on this page. 
	 * @return
	 */
	public Iterator<Tuple> iterator() {
		//your code here
		ArrayList<Tuple> al = new ArrayList<Tuple>();
		for (int i = 0; i < this.getNumSlots(); i++) {
			//System.out.println("slot " + i + " occupied? " + this.slotOccupied(i));
			if (this.slotOccupied(i)) {
				al.add(this.tuples[i]);
			}
		}
		//System.out.println("arraylist size = " + al.size());
		return al.iterator();
	}

}
